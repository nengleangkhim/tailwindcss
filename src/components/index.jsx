import React from 'react'

export default function index() {
    return (

        <div class="grid grid-cols-2">
            <div>
                <div className="p-10 min-h-screen flex items-center justify-center bg-red-400">
                    <div className="animate-bounce">
                        <div className="h-36 w-36 bg-red-600 rounded-full flex items-center justify-center">
                            <span className="text-9xl">🐷</span>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <div className="p-10 min-h-screen flex items-center justify-center bg-blue-400">
                    <div className="animate-spin-slow">
                        <div className="h-36 w-36 bg-blue-600 rounded-full flex items-center justify-center">
                            <span className="text-9xl">🌎</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
